package com.toplion.wisehome.util;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Hex;

import com.toplion.wisehome.util.HanziToPinyin.Token;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

public class Tools {

	/** 
	* 将两个ASCII字符合成一个字节； 
	* 如："EF"--> 0xEF 
	* @param src0 byte 
	* @param src1 byte 
	* @return byte 
	*/ 
	public  byte uniteBytes(byte src0, byte src1) { 
	byte _b0 = Byte.decode("0x" + new String(new byte[]{src0})).byteValue(); 
	_b0 = (byte)(_b0 << 4); 
	byte _b1 = Byte.decode("0x" + new String(new byte[]{src1})).byteValue(); 
	byte ret = (byte)(_b0 ^ _b1); 
	return ret; 
	} 

	private int bytesToInt( byte[] bytes ) {
		int result = 0;
		for (int i=bytes.length-1; i>=0; i--) 
		{
		   result =  (int) (result + ( (int)( bytes[i] & 0xFF) ) * Math.pow(2, 8*i));
		}
		return result;
	}
	
	public int decodeBCD(byte b) {
		return Integer.parseInt(new String ( Hex.encodeHex( new byte[]{ b } ) ) ) ;
	}
	
	/*
	 * parameter  CodeString: 16 bytes, from XML
	 * output: 18 bytes
	 */
	public	String  Parameterstring(String CodeString)
	{		
		Log.e("Parameterstring","code=" + CodeString);
		if(CodeString.length()!=16) return null;
		//bytes 18 bytes
		byte  bytes[] = new byte[]{0x33, 0x43, 0x30 ,0x32, 0x30, 0x30, 0x30 ,0x30 ,0x45, 0x44, 0x30 ,0x31, 0x30 ,0x33, 0x44 ,0x43 ,0x46 ,0x35};
		int t =0;
		String s =CodeString;
		
		  for (t=0;t<CodeString.length();t++){		     
			  bytes[t] = CodeString.getBytes()[t];					  
					 
		    }
		 
		  String str2 = s;
			  
		  int i;
		    int v;
		    int total=0;
		    for (i=0;i<str2.length();i+=2) {		    	
		     	v =   uniteBytes(str2.getBytes()[i],str2.getBytes()[i+1]);		 
		        total=total+(int)(v&0xFF);
		    }
		    s = String.format("%x",total);		   
		    s =   s.substring(s.length()-2);
		    s = s.toUpperCase();

		    int value = (int)(uniteBytes(s.getBytes()[0],s.getBytes()[1]) & 0xFF);
		    s = String.format("%02x",256-value);	
		    s = s.toUpperCase();
		    
		    str2 = s;
		    
		    bytes[16]=str2.getBytes()[0];
		    bytes[17]=str2.getBytes()[1];
		    
		    String newcode = new String(bytes);
		    Log.e("Parameterstring","new code=" + newcode);
		    return newcode;
		    
		
	}
	
	public static int getImageNameID(Context mContext,String input) {
		String replaced =  input;
		String chinese = "[a-zA-Z_0-9]";
        Pattern p = Pattern.compile(chinese);
        Matcher m = p.matcher(replaced);
        replaced = m.replaceAll("");
        
        String pattern = "[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]";
        p = Pattern.compile(pattern);
        m = p.matcher(replaced);
        replaced = m.replaceAll(""); 
        
        String imangName = PinYin.getPinYin(replaced);
		Resources res= mContext.getResources(); 
		int i=res.getIdentifier(imangName,"drawable","com.toplion.wisehome");
		return i;
	}
	
}
