package com.toplion.wisehome.util;

import java.util.ArrayList;

import android.util.Log;

import com.toplion.wisehome.util.HanziToPinyin.Token;

public class PinYin {
	//汉字返回拼音，字母原样返回，都转换为小写(默认取得的拼音全大写)  
	public static String getPinYin(String input) {
		ArrayList<Token> tokens = HanziToPinyin.getInstance().get(input);
		StringBuilder sb = new StringBuilder();
		if (tokens != null && tokens.size() > 0) {
			for (Token token : tokens) {
				if (Token.PINYIN == token.type) {
					sb.append(token.target);
				} else {
					sb.append(token.source);
				}
			}
		}
		return sb.toString().toLowerCase();
	}
}
