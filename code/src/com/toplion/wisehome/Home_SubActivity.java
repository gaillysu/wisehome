package com.toplion.wisehome;

import java.util.ArrayList;
import java.util.List;

import com.toplion.wisehome.adapter.LightAdapter;
import com.toplion.wisehome.adapter.QueryAdapter;
import com.toplion.wisehome.adapter.RoomFeatureAdapter;
import com.toplion.wisehome.entity.LightContent;
import com.toplion.wisehome.entity.QueryContent;
import com.toplion.wisehome.entity.RoomContent;
import com.toplion.wisehome.global.Global;
import com.toplion.wisehome.model.Floor;
import com.toplion.wisehome.model.Room;
import com.toplion.wisehome.model.Sensor;


import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Home_SubActivity extends Activity {

	LightAdapter adapter;
	private ListView list;
	private List<LightContent> dlist;
	private List<RoomContent> rlist;
	RoomFeatureAdapter roomadapter;
	
	String type;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_subhome);        
        list = (ListView) findViewById(R.id.lvlist);        
        type = getIntent().getStringExtra("type");
        
        TextView title = (TextView)findViewById(R.id.txtTitle);
        title.setText(type);

        ImageButton btnNav = (ImageButton) findViewById(R.id.btnNav);
        btnNav.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				finish();		
			}
		});
       // adapter=new LightAdapter(this, getData());
       // list.setAdapter(adapter);
        roomadapter = new RoomFeatureAdapter(this,type);
        list.setAdapter(roomadapter);     
    }

    private List<LightContent> getData(){
    	
    	dlist=new ArrayList<LightContent>();		
    
    	if(Global.getInstance().building.getmFloors().isEmpty()) 
    	{
    		Toast.makeText(this, "XML�ļ�����", Toast.LENGTH_LONG).show();
    		finish();
    		return dlist;
    	}

    	Floor floor = null ;
    	for(Floor fr : Global.getInstance().building.getmFloors())
       {
    	   if(fr.getName().equals(type))
    	   {
    		   floor  = fr;
    		   break;
    	   }
       }
	
    	if(floor == null) return dlist;
    
		for(Room room:floor.getmRooms())
		{
			for(Sensor sensor:room.getmSensors())
			{				
				dlist.add(new LightContent(room.getName()+"-"+sensor.getName(),false,sensor.getCode(),sensor.getQueryCode()));
			}
		}
		
		return  dlist;
	}

}
