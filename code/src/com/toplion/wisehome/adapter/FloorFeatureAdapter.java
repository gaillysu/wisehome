package com.toplion.wisehome.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import szy.sdklib.SzySDKlib;

import com.toplion.wisehome.HomeActivity;
import com.toplion.wisehome.Home_SubActivity;
import com.toplion.wisehome.MainActivity;
import com.toplion.wisehome.R;
import com.toplion.wisehome.Room_SubActivity;
import com.toplion.wisehome.entity.LightContent;
import com.toplion.wisehome.global.Global;
import com.toplion.wisehome.model.Floor;
import com.toplion.wisehome.model.Room;
import com.toplion.wisehome.model.Sensor;


import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.toplion.wisehome.util.PinYin;

public class FloorFeatureAdapter extends BaseAdapter {
	private static final String CATEGORY = "no.nordicsemi.android.nrftoolbox.LAUNCHER";

	private final Context mContext;

	private final LayoutInflater mInflater;
	private final List<FloorFeature> mFloors =new ArrayList<FloorFeature>();
    private String type = "灯光";
    
	public FloorFeatureAdapter(final Context context) {
		mContext = context;
		mInflater = LayoutInflater.from(context);

    	for(Floor fr : Global.getInstance().building.getmFloors())
       {
    		String imangName = PinYin.getPinYin(fr.getName());
    		Resources res= mContext.getResources(); 
    		int i=res.getIdentifier(imangName,"drawable","com.toplion.wisehome");
    		int x = 1;
    		for (FloorFeature flor : mFloors){
    			if(fr.getName().indexOf(flor.name) != -1){
    				x = 0;
    				break;
    			}
    		}
    		if(x == 1){
    			if (i==0){
        			mFloors.add(new FloorFeature(R.drawable.tongyong,fr.getName()));
        		}else{
        			mFloors.add(new FloorFeature(i,fr.getName()));
        		}	
    		}
    		
    		
    		Log.i("test",fr.getName());
    		/*
    		if(fr.getName().equals("灯光"))
    			mFloors.add(new FloorFeature(R.drawable.dengguang,fr.getName()));
    		else if(fr.getName().equals("窗帘"))
        		mFloors.add(new FloorFeature(R.drawable.chuanglian,fr.getName()));
    		else if(fr.getName().equals("空调"))
        		mFloors.add(new FloorFeature(R.drawable.kongtiao,fr.getName()));
    		else if(fr.getName().equals("家电"))
        		mFloors.add(new FloorFeature(R.drawable.jiadian,fr.getName()));
    		else if(fr.getName().equals("TV"))
        		mFloors.add(new FloorFeature(R.drawable.tv,fr.getName()));
    		else if(fr.getName().equals("安防"))
        		mFloors.add(new FloorFeature(R.drawable.anfang,fr.getName()));
    		else if(fr.getName().equals("区域"))
        		mFloors.add(new FloorFeature(R.drawable.quyu,fr.getName()));
    		else if(fr.getName().equals("场景"))
        		mFloors.add(new FloorFeature(R.drawable.changjing,fr.getName()));
    		else if(fr.getName().equals("电锁"))
        		mFloors.add(new FloorFeature(R.drawable.diansuo,fr.getName()));
    		else if(fr.getName().equals("监控"))
        		mFloors.add(new FloorFeature(R.drawable.jiankong,fr.getName()));
    		else
    			mFloors.add(new FloorFeature(R.drawable.icon_default,fr.getName()));
    			*/
       }

	}

	@Override
	public int getCount() {
		return mFloors.size();
	}

	@Override
	public Object getItem(int position) {
		return mFloors.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			view = mInflater.inflate(R.layout.feature_icon, parent, false);

			final ViewHolder holder = new ViewHolder();
			holder.view = view;
			holder.icon = (ImageView) view.findViewById(R.id.icon);
			holder.label = (TextView) view.findViewById(R.id.label);
			view.setTag(holder);
		}

	
		final ViewHolder holder = (ViewHolder) view.getTag();
		holder.icon.setImageDrawable(mContext.getResources().getDrawable(mFloors.get(position).icon));
		holder.label.setTextColor(mContext.getResources().getColor( android.R.color.holo_green_dark));
		holder.label.setText(mFloors.get(position).name);
		holder.view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {						
				if(mFloors.get(position).name.equals("监控"))
				{
					String [] ips ={"login.4006043110.cn","login.4006043110.com","login.53t01.com","220.181.120.243"}; //可以是一个或者多个，域名和IP皆可
					int nPort = 8006;
					//szbrt,123450
					String sUser = PreferenceManager.getDefaultSharedPreferences(mContext).getString("USERNAME", "");
					String sPassword = PreferenceManager.getDefaultSharedPreferences(mContext).getString("PASSWORD", "");
					if(sUser.equals("") || sPassword.equals(""))
					{
						Toast.makeText(mContext, "请先设定视频用户及密码", Toast.LENGTH_LONG).show();
						return;
					}
					SzySDKlib.startActivity((Activity) mContext, ips, nPort, sUser, sPassword);
				}
				else
				{
				Intent intent = new Intent(mContext, Home_SubActivity.class);
				intent.putExtra("type",mFloors.get(position).name);
				mContext.startActivity(intent);
				}
			}
		});

		return view;
	}

	private class ViewHolder {
		private View view;
		private ImageView icon;
		private TextView label;
	}
	private class FloorFeature {
	    private int icon;
	    private String name;
	    
	    FloorFeature(int icon,String name)
	    {
	    	this.icon = icon;
	    	this.name = name;
	    }
	}
}
