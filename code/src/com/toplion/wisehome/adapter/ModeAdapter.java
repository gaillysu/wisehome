package com.toplion.wisehome.adapter;

import java.util.List;



import com.toplion.wisehome.R;
import com.toplion.wisehome.entity.ModeContent;
import com.toplion.wisehome.entity.QueryContent;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ModeAdapter extends BaseAdapter {

	private Context context;
	private List<ModeContent> list;
	
	public ModeAdapter(Context context, List<ModeContent> list) {
		super();
		this.context = context;
		this.list = list;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View layoutview = null;
		LayoutInflater inflater = LayoutInflater.from(context);
		
		if(convertView == null)
		{
			layoutview =  inflater.inflate(R.layout.query_item, null);			
		}
		else
			layoutview = convertView;
		
		ImageView image = (ImageView) layoutview.findViewById(R.id.ivImage);
		image.setBackgroundResource(list.get(position).getIcon());
		TextView tv = (TextView) layoutview.findViewById(R.id.txtName);
		tv.setText(list.get(position).getName());		
		return layoutview;
	}

}
