package com.toplion.wisehome.entity;

public class RoomContent {	
	private String name;
	private String code;
	
	public RoomContent( String name,String code)
	{
		this.name=name;
		this.code = code;
	}
	public String getName(){return name;}
	public String getCode() {return code;}
}
