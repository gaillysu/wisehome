package com.toplion.wisehome.entity;

public class SecurityContent {
	private int id;
	private int icon;
	private String name;
	private boolean status;
	
	
	
	public final static int inside=1;
	public final static int outside =2;
	
	public SecurityContent(int outside,int icon,String name,boolean status)
	{
		this.id =outside;
		this.icon = icon;
		this.name=name;
		this.status = status;
	}
	public String getName(){return name;}
	public int getIcon() {return icon;}
	public int getId(){return id;}
	public boolean getStatus() {return status;}
	
}
