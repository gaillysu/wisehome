package com.toplion.wisehome;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.view.Window;
import android.widget.Toast;

public class SplashActivity extends Activity {
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
       
		setContentView(R.layout.activity_splash);
		new Handler().postDelayed(new  Runnable(){

			@Override
			public void run() {
		       	Intent intent = new Intent();
				intent.setClass(SplashActivity.this,MainActivity.class);						
				intent.setClass(SplashActivity.this,LoginActivity.class);				
				startActivity(intent);
						
				SplashActivity.this.finish();				
			}
			
		}, 3000);
	}
	
	


}
