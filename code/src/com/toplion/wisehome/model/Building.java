package com.toplion.wisehome.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Building  extends BaseElement{
	
	private List<Floor> mFloors = new ArrayList<Floor>();

	public List<Floor> getmFloors() {
		return mFloors;
	}

	public void setmFloors(List<Floor> mFloors) {
		this.mFloors = mFloors;
	}
	
	public JSONObject ToJson()
	{
		JSONObject json = super.ToJson();
		try {
			json.put("Floors",new JSONArray(mFloors) );
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return json;
	}
	
}
