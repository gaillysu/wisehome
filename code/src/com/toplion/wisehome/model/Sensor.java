package com.toplion.wisehome.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Sensor extends BaseElement {

	private String Code;
	private String QueryCode;
	
	public String getCode() {
		return Code;
	}
	public void setCode(String code) {
		Code = code;
	}
	public String getQueryCode() {
		return QueryCode;
	}
	public void setQueryCode(String queryCode) {
		QueryCode = queryCode;
	}
	
	public JSONObject ToJson()
	{
		JSONObject json =super.ToJson();
		
		try {
			json.put("Code", Code);
			json.put("QueryCode", QueryCode);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return json;
	}
}
