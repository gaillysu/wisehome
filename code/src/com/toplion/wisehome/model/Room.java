package com.toplion.wisehome.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Room extends BaseElement{

	private List<Sensor> mSensors = new ArrayList<Sensor>();

	public List<Sensor> getmSensors() {
		return mSensors;
	}

	public void setmSensors(List<Sensor> mSensors) {
		this.mSensors = mSensors;
	}
	
	public JSONObject ToJson()
	{
		JSONObject json = super.ToJson();
		try {
			json.put("Sensors", new JSONArray(mSensors));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}
}
