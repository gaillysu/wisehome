package com.toplion.wisehome.network;

import org.json.JSONObject;


public class SendCommandRequest extends RequestImpl<JSONObject> {

	private String cmd;
	public SendCommandRequest(String cmd,ServerResponseListener<JSONObject> callback) {
		super(callback);
		this.cmd = cmd;
	}

	@Override
	public String getService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONObject getParams() throws Exception {
		// TODO Auto-generated method stub
		return new JSONObject().put("cmd",cmd) ;
	}

	@Override
	public void handleResponse(JSONObject response) {
		// TODO Auto-generated method stub
		mCallback.onServerResponse(response);
	}

}
