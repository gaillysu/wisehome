package com.toplion.wisehome.network;


public interface ConnectManager {
	   
	public void DownloadXML(Request r);
	public void ConnectServer(Request r);
	public void DisconnectServer(Request r);
	public void SendCommand(Request r);
	public void SendCommandDelay(Request r,int ms);
	public void UdpConnectServer(Request r);
	public void UdpDisconnectServer();
	public void UdpSendCommand(Request r);
}
