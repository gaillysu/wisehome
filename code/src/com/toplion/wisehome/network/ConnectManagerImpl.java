package com.toplion.wisehome.network;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.List;

import org.apache.commons.codec.binary.Hex;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Xml;
import android.webkit.WebView.FindListener;
import android.widget.Toast;

import com.toplion.wisehome.MainActivity;
import com.toplion.wisehome.global.Global;
import com.toplion.wisehome.model.Building;
import com.toplion.wisehome.model.Floor;
import com.toplion.wisehome.model.Room;
import com.toplion.wisehome.model.Sensor;
import com.toplion.wisehome.util.AccountSaveToLocal;


public class ConnectManagerImpl implements ConnectManager {
	
	//:8300/network.xml
	public static final String strConfigFile = "http://hbusdemo.oicp.net";
	public static final String strConfigPort = "8300";
	public static final String fileName = "/network.xml";
	public static final String strlocalFile = "/sdcard/network.xml";
	public static String strServerName = "hbusdemo.oicp.net";
	public static final int iPort = 8100; // hard code
	
	public static final String strUdpServerName = "255.255.255.255";
	public static final int iUdpPort = 8888;
	
	public static final String ACCOUNT = "account";
	public static final String ACCOUNTUSERNAMESAVELOCALKEY = "accountUsernameSaveLocalKey";//用来保存登陆时候账号到本地的KEY
	public static final String ACCOUNTPASSWORDSAVELOCALKEY = "accountPasswordSaveLocalKey";//用来保存登陆时候密码到本地的KEY
	
	Socket socket = null;
	BufferedWriter bw = null;
	BufferedReader br = null;
	
	//UDP parameter
    private byte[] buffer = new byte[1024];    
    private DatagramSocket ds = null;
	
	public ConnectManagerImpl(){}
	//Classic singleton
	private static ConnectManager sInstance = null;
	public static ConnectManager getInstance() {
					if(null == sInstance )
					{
						sInstance = new ConnectManagerImpl();
					}
					return sInstance;
				}
	
	//END - Classic singleton
			
	/** The Handler of the ui thread. Because it is more polite to call callbacks in the main thread. */
	final Handler mUiThread = new Handler(Looper.getMainLooper());
	
	@Override
	public void ConnectServer(final Request r) {
		if(r.isUdpRequest())
		{
			UdpConnectServer(r);
			return;
		}
		new Thread(){
			@Override
			public void run() {				
				    try {
						socket = new Socket(strServerName, iPort);
					
			            socket.setSoTimeout(10000);
			            
			            br = new BufferedReader(new InputStreamReader(
			                    socket.getInputStream()));
			            
			            bw = new BufferedWriter(new OutputStreamWriter(
			                    socket.getOutputStream()));		
			            
			            mUiThread.post(new Runnable(){
							@Override
							public void run() {
								try {
									r.handleResponse(new JSONObject().put("status","已成功连接服务器"));
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}								
							}});
			            
					} catch (final UnknownHostException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						mUiThread.post(new Runnable(){
							@Override
							public void run() {
								r.onError(e);								
							}});
					} catch (final IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						mUiThread.post(new Runnable(){
							@Override
							public void run() {
								r.onError(e);								
							}});
					}
			}
						
		}.start();

	}

	@Override
	public void DisconnectServer(final Request r) {
		
		if(r.isUdpRequest())
		{
		UdpDisconnectServer();
		return;
		}
		new Thread(new Runnable(){

			@Override
			public void run() {
				if(socket == null) return;
				try {
					br.close();
					bw.close();
					socket.close();
					mUiThread.post(new Runnable(){
						@Override
						public void run() {
							try {
								r.handleResponse(new JSONObject().put("status","已断开服务器连接"));
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}							
						}});
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					mUiThread.post(new Runnable(){
						@Override
						public void run() {
							r.onError(e);								
						}});
				}
				
			}}).start();

	}
	@Override
	public void SendCommand(final Request r)
	{
		QueuedMainThreadHandler.getInstance().post(new Runnable(){
			@Override
			public void run() {
				SendCommandByQueue(r);
			}			
		});
	}
	@Override
	public void SendCommandDelay(final Request r,int ms)
	{
		QueuedMainThreadHandler.getInstance().postDelayed(new Runnable(){
			@Override
			public void run() {
				SendCommandByQueue(r);
			}			
		},ms);
	}

	private void SendCommandByQueue(final Request r) {		
		if(r.isUdpRequest())
		{
			UdpSendCommand(r);
			//QueuedMainThreadHandler.getInstance().next();
			return;
		}
		new Thread(new Runnable(){

			@Override
			public void run() {
		    
				if(socket == null) {
					mUiThread.post(new Runnable(){
						@Override
						public void run() {
							r.onError(new Throwable("socket is disconnected") );								
						}});
					return;
				}				
				try {	
					
					bw.write((char)0x02);					
					bw.write(r.getParams().getString("cmd"));					
					bw.write((char)0x03);					
					bw.flush();
										
					Log.w("ConnectManagerImpl", "Send request:02"+r.getParams().getString("cmd") + "03");
					
					//need get server's response, make sure this command excute sucess or failure.
					char []ch= new char[1];
					byte [] temp = new byte[100];
					int length = -1;			
					int total = 0;
					Boolean START = false;
					Boolean END = false;
					while((length = br.read(ch))>0 && total<temp.length)					
					{	
						for(int i=0;i<length;i++) temp[total+i] = (byte)(ch[i]);
						total = total + length;
						
						if(ch[0] == (char)0x02) START = true;
						else if(ch[0] == (char)0x03 && START) 
						{
							   END = true;							   
							   break;
						}					   
					}
					//check response packets is vaild???
					
					//end check
					byte []receivebuffer = new byte[total];
					for(int k=0;k<total;k++) receivebuffer[k] =temp[k];					
		            final String strResponse = new String(Hex.encodeHex(receivebuffer));
		            Log.i("ConnectManagerImpl",  total + " bytes Receive:"+ strResponse);
		            
					if(r.backgroundThread())
					{						
						new Thread(new Runnable(){
							@Override
							public void run() {						
								
									mUiThread.post(new Runnable(){
										@Override
										public void run() {
											try {
												r.handleResponse(new JSONObject().put("response",strResponse));
												QueuedMainThreadHandler.getInstance().next();
											} catch (JSONException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}								
										}});															
							}							
						}).start();
					}
					else
					mUiThread.post(new Runnable(){
						@Override
						public void run() {
							try {						
								r.handleResponse(new JSONObject().put("status","write success:"+r.getParams().getString("cmd")));
								QueuedMainThreadHandler.getInstance().next();
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}	
							 catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
						}});
					
					
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					mUiThread.post(new Runnable(){
						@Override
						public void run() {
							r.onError(e);								
						}});
				}	catch (final JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					mUiThread.post(new Runnable(){
						@Override
						public void run() {
							r.onError(e);								
						}});
					
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();	
					mUiThread.post(new Runnable(){
						@Override
						public void run() {
							r.onError(e);								
						}});
				}		
				
			}}).start();

	}
	
	@Override
	public void DownloadXML(final Request r) {
		
		new Thread(new Runnable(){
			@Override
			public void run() {
				boolean error = false;
				HttpURLConnection connection = null;
				//RandomAccessFile randomAccessFile = null;
				InputStream is = null;
			    Building building =null ;
			    Floor floor = null;
			    Room room = null;
			    Sensor sensor = null;
			    
				try {
					URL url = new URL(r.getParams().getString("url"));
					connection = (HttpURLConnection) url.openConnection();
					connection.setConnectTimeout(5000);
					connection.setRequestMethod("GET");			
									
					final int fileSize = connection.getContentLength();			
					
					is = connection.getInputStream();
					/*
					byte[] buffer = new byte[4096];
					int length = -1;
					int compeleteSize = 0;
					
					randomAccessFile = new RandomAccessFile(new File(r.getParams().getString("localfile")), "rwd");
					
					while ((length = is.read(buffer)) != -1) {
						randomAccessFile.write(buffer, 0, length);
						compeleteSize += length;		
						final int finished = compeleteSize;
						// 用消息将下载信息传给进度条，对进度条进行更新	
						//		
					}
					*/
								
					XmlPullParser parser = Xml.newPullParser();					
					parser.setInput(is, "UTF-8");
					
					int eventCode = 0;
				    String element = null,name = null,value = null;
				    boolean running = true;				   
				    				    
				    while(running)
				    {
				    	try {
							eventCode = parser.next();
						} catch (XmlPullParserException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}				    	
				    	switch(eventCode){
						case XmlPullParser.END_DOCUMENT:
							running = false;									
							break;				
						case XmlPullParser.START_TAG:
							element =  parser.getName();
							if(element == null) break;
							//value = parser.getText();
							
							if(element.equals("Building"))
							{						
								building = Global.getInstance().building;
								building.setName(element);
							}
							else if(element.equals("Floor"))
							{						
								 floor = new Floor();
								 floor.setId(parser.getAttributeValue(null,"Id"));
								 floor.setName(parser.getAttributeValue(null,"Name"));								 
								 building.getmFloors().add(floor);
							}
							else if(element.equals("Room"))
							{						
								room = new Room();
								room.setId(parser.getAttributeValue(null,"Id"));
								room.setName(parser.getAttributeValue(null,"Name"));
								
								floor.getmRooms().add(room);
							}
							else if(element.equals("Sensor"))
							{	
								sensor = new Sensor();
								sensor.setId(parser.getAttributeValue(null,"Id"));
								sensor.setName(parser.getAttributeValue(null,"Name"));
								sensor.setCode(parser.getAttributeValue(null,"Code"));
								sensor.setQueryCode(parser.getAttributeValue(null,"QueryCode"));
								
								room.getmSensors().add(sensor);
							}	
							break;
						case XmlPullParser.TEXT:
							break;
						case XmlPullParser.END_TAG:										
							element = null;
							break;	
				    	}
				    }
					
					
				} catch (final Exception e) {
					e.printStackTrace();
					mUiThread.post(new Runnable(){
						@Override
						public void run() {
							r.onError(e);								
						}});
					error = true;
				} finally {
					try {
					
						 final JSONObject json = building.ToJson();
						//randomAccessFile.close();
						is.close();					
						connection.disconnect();	
						if(error == false)
						{								
							mUiThread.post(new Runnable(){
								@Override
								public void run() {
									try {
										r.handleResponse(new JSONObject().put("status", "下载数据成功!"));
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}		
								}});							
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}}).start();
		
	}

	@Override
	public void UdpConnectServer(final Request r) {
		new Thread(){
			@Override
			public void run() {				
				    try {
				    	ds = new DatagramSocket();					
			            ds.setSoTimeout(5000);
			            
			            DatagramPacket dp = new DatagramPacket(new byte[] {0x2C,(byte) 0xFF,(byte) 0xFF,(byte) 0xFF,(byte) 0xFF,0x00,0x00,0x00,0x00}, 9, InetAddress    
			                    .getByName(strUdpServerName), iUdpPort);    
			            ds.send(dp);
			           
			            new Thread(new Runnable(){
							@Override
							public void run() {						
								try {
									buffer = new byte[1024];
									DatagramPacket dp = new DatagramPacket(buffer, buffer.length);    
							        ds.receive(dp); 
							        if(dp.getAddress().equals(strUdpServerName))
							        { 	
									final String strResponse = new String(dp.getData(), 0, dp.getLength());	
									
									if(strResponse.equals(new String("28FFFFFFFF00000000"))) //成功	
										
									mUiThread.post(new Runnable(){
										@Override
										public void run() {
											try {
												r.handleResponse(new JSONObject().put("status","已成功UDP连接服务器"));
											} catch (JSONException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}							
										}});
									else 
										mUiThread.post(new Runnable(){
										@Override
										public void run() {
											try {
												r.handleResponse(new JSONObject().put("status",strResponse));
											} catch (JSONException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}							
										}});
									
							        }
							        else
							        {
							        	mUiThread.post(new Runnable(){
											@Override
											public void run() {
												r.onError(new IOException("Received packet from an unknown source"));							
											}});
							        	
							        }
							        
								} catch (final IOException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
									mUiThread.post(new Runnable(){
										@Override
										public void run() {
											r.onError(e1);								
										}});
								}					
							}							
						}).start();			            
			       
					} catch (final IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						mUiThread.post(new Runnable(){
							@Override
							public void run() {
								r.onError(e);								
							}});
					}
			}
						
		}.start();
		
	}
	
	@Override
	public void UdpSendCommand(final Request r) {
		new Thread(new Runnable(){

			@Override
			public void run() {
		    
				if(ds == null) {
					mUiThread.post(new Runnable(){
						@Override
						public void run() {
							r.onError(new Throwable("UDP socket is disconnected") );								
						}});
					return;
				}
				
				try {
					if(r.getParams().getString("cmd").getBytes().length !=18) {
						mUiThread.post(new Runnable(){
							@Override
							public void run() {
								r.onError(new Throwable("Command Data length error(!=18)") );								
							}});
						return;
					}
				} catch (JSONException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
					return;
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
					return;
				}
								
				try {					
					
					byte[] sendbuffer = new byte[20];
					
					sendbuffer[0] = 0x02;
					for(int i=0;i<18;i++)
					{
						sendbuffer[i+1] = r.getParams().getString("cmd").getBytes()[i];
					}
					sendbuffer[19]= 0x03;				
					
					 DatagramPacket dp = new DatagramPacket(sendbuffer, sendbuffer.length, InetAddress    
			                    .getByName(strUdpServerName), iUdpPort);    
			         ds.send(dp);
							
					//need get server's response, make sure this command excute sucess or failure.
					if(r.backgroundThread())
					{
						new Thread(new Runnable(){
							@Override
							public void run() {						
								try {					
									buffer = new byte[1024];
									DatagramPacket dp = new DatagramPacket(buffer, buffer.length);    
							        ds.receive(dp); 
							        if(dp.getAddress().equals(strUdpServerName))
							        { 	
									final String strResponse = new String(dp.getData(), 0, dp.getLength());	
															        
									mUiThread.post(new Runnable(){
										@Override
										public void run() {
											try {
												r.handleResponse(new JSONObject().put("response",strResponse));
											} catch (JSONException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}								
										}});
									
							        }
							        else
							        {
							        	mUiThread.post(new Runnable(){
											@Override
											public void run() {
												r.onError(new IOException("Received packet from an unknown source"));								
											}});
							        }
								} catch (IOException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
									r.onError(e1);
								}								
							}							
						}).start();
					}
					else
					mUiThread.post(new Runnable(){
						@Override
						public void run() {
							try {							
								r.handleResponse(new JSONObject().put("status","write success:"+r.getParams().getString("cmd")));
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}	
							 catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
						}});
					
					
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					mUiThread.post(new Runnable(){
						@Override
						public void run() {
							r.onError(e);								
						}});
				}	catch (final JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					mUiThread.post(new Runnable(){
						@Override
						public void run() {
							r.onError(e);								
						}});
					
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();	
					mUiThread.post(new Runnable(){
						@Override
						public void run() {
							r.onError(e);								
						}});
				}		
				
			}}).start();

		
	}

	@Override
	public void UdpDisconnectServer() {
		// TODO Auto-generated method stub
		try {    
            ds.close();    
        } catch (Exception ex) {    
            ex.printStackTrace();    
        }    
	}

	

	

}
