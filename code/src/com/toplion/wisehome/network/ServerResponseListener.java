
package com.toplion.wisehome.network;


public interface ServerResponseListener<T> {

	/**
	 * A valid server response have been given and decoded by the Request object, now it is return in a model readable way.
	 * Note that this function requires that T is know, so it should only be called with care
	 * @param a validResponse
	 */
	public void onServerResponse(T validResponse);
	
	/**
	 * This function is called when an error occured.
	 * Error mean, an invalid request or a conenction error.
	 * In particular, a bad password/username combination shouldn't be treated as an error.
	 * @param error
	 */
	public void onServerError(Throwable error);
	
}
