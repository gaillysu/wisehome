package com.toplion.wisehome.network;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Separated from the interface, this class is a convenience class for all the most basic functionnalities of a request
 * It's main purpose is to : 
 * 1-Store most used constants
 * 2-Prevent the interface from having a setCallback method
 * @author Hugo
 *
 * @param <T>
 */
/*package*/ abstract class RequestImpl<T> implements Request {
	/**
	 * The callback, to be called when the server responds.
	 * Being a valid response or an Error
	 */
	final ServerResponseListener<T> mCallback;

	static final String RESPONSE_KEY = "status";
	
	static final String CHECKSUM_KEY = "checkSumKey";
	
	static final String TOKEN_KEY = "tokenKey";

	/*
	 * Every subclasses should give a callback.
	 * We do this as to avoid Typing on the Request interface.
	 * That's better than setting the callback
	 */
	RequestImpl(ServerResponseListener<T> callback) {
		mCallback = callback;
	}

	/*
	 * (non-Javadoc)
	 * @see com.imaze.bluetooth.le.cloud.request.Request#onError(java.lang.Throwable)
	 */
	@Override
	public void onError(Throwable error) {
		mCallback.onServerError(error);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.imaze.bluetooth.le.cloud.request.Request#backgroundThread()
	 */
	@Override
	public boolean backgroundThread() {
		return mCallback instanceof BackgroundServerResponseListener;
	}
	
	@Override
	public boolean isUdpRequest() {
		return false;
	}
}
