package com.toplion.wisehome;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.json.JSONException;
import org.json.JSONObject;


import com.toplion.wisehome.adapter.LightAdapter;
import com.toplion.wisehome.adapter.QueryAdapter;
import com.toplion.wisehome.entity.LightContent;
import com.toplion.wisehome.entity.QueryContent;
import com.toplion.wisehome.global.Global;
import com.toplion.wisehome.model.Floor;
import com.toplion.wisehome.model.Room;
import com.toplion.wisehome.model.Sensor;
import com.toplion.wisehome.network.BackgroundServerResponseListener;
import com.toplion.wisehome.network.ConnectManagerImpl;
import com.toplion.wisehome.network.SendCommandRequest;
import com.toplion.wisehome.network.ServerResponseListener;
import com.toplion.wisehome.util.Tools;


import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Room_SubActivity extends Activity implements OnClickListener {

	LightAdapter adapter;
	private ListView list;
	private List<LightContent> dlist;
	final String hongwaiDevice = "29,0B,0C";
	Boolean isHongWai = false;
	
	String type;
	String name;
	Boolean poweronoff = false;
	int temperature = 25;
	TextView temperature_view;
	private static HashMap<String, Button> controllerpanel = new HashMap<String, Button>();
	private static HashMap<String, ImageView> controllerpanel_tv = new HashMap<String, ImageView>();
	//private static HashMap<String, ImageView> controllerpanel_curtain = new HashMap<String, ImageView>();
	//private static HashMap<String, ImageView> controllerpanel_xiaomihezi = new HashMap<String, ImageView>();
	//private static HashMap<String, ImageView> controllerpanel_gongfang = new HashMap<String, ImageView>();
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        type = getIntent().getStringExtra("type");
        name =  getIntent().getStringExtra("name");
        dlist = getData();
        
        for(LightContent light :dlist){
        	final String code = light.getCode();
        	String querCoder = code.substring(2, 4);
        	final String[] codearray = hongwaiDevice.split(",");
        	for(String str:codearray){
        		if(str.equals(querCoder)){
        			isHongWai = true;
        			break;
        		}
        	}
        	if(isHongWai){
        		break;
        	}
        }
        
        if (type.equals("空调"))
        {
        	 setContentView(R.layout.remote_control);
        	 controllerpanel.put("on",(Button) findViewById(R.id.openPower));
        	 controllerpanel.put("off",(Button) findViewById(R.id.ClosePower));
        	 controllerpanel.put("cool",(Button) findViewById(R.id.ch_button));
        	 controllerpanel.put("hot",(Button) findViewById(R.id.hc_button));
        	 controllerpanel.put("auto",(Button) findViewById(R.id.auto_button));
        	 controllerpanel.put("wind",(Button) findViewById(R.id.fcu_button));
        	 controllerpanel.put("windmin",(Button) findViewById(R.id.wind_minButton));
        	 controllerpanel.put("windin",(Button) findViewById(R.id.wind_inButton));
        	 controllerpanel.put("windmax",(Button) findViewById(R.id.wind_maxButton));
        	 controllerpanel.put("up",(Button) findViewById(R.id.value_up_button));
        	 controllerpanel.put("down",(Button) findViewById(R.id.value_down_button));
        	 /*
        	 controllerpanel.get("onoff").setOnClickListener(this);
        	 controllerpanel.get("cool").setOnClickListener(this);   	
             controllerpanel.get("hot").setOnClickListener(this);             
             controllerpanel.get("auto").setOnClickListener(this);
        	 controllerpanel.get("wind").setOnClickListener(this);   	
             controllerpanel.get("windmin").setOnClickListener(this);
             controllerpanel.get("windin").setOnClickListener(this);
        	 controllerpanel.get("windmax").setOnClickListener(this);   	
             controllerpanel.get("up").setOnClickListener(this);
             controllerpanel.get("down").setOnClickListener(this);
             */
        	 for (Button bt:controllerpanel.values()) bt.setOnClickListener(this);
        	 
             temperature_view = (TextView)findViewById(R.id.temp_ValueTextView);
             temperature_view.setText(""+temperature + "℃" );
        }else if (type.equals("电视") || type.equals("TV") || type.equals("小米盒子") || type.equals("功放") ||isHongWai){
        	 setContentView(R.layout.tv_control);
        	         	 
        	 controllerpanel_tv.put("signal", (ImageView) findViewById(R.id.tv_signal));
        	 controllerpanel_tv.put("power", (ImageView) findViewById(R.id.tv_power));
        	 controllerpanel_tv.put("left", (ImageView) findViewById(R.id.tv_leftkey));
        	 controllerpanel_tv.put("ok", (ImageView) findViewById(R.id.tv_okkey));
        	 controllerpanel_tv.put("right", (ImageView) findViewById(R.id.tv_rightkey));
        	 controllerpanel_tv.put("up", (ImageView) findViewById(R.id.tv_upkey));
        	 controllerpanel_tv.put("down", (ImageView) findViewById(R.id.tv_dwonkey));
        	 controllerpanel_tv.put("menu", (ImageView) findViewById(R.id.tv_home));
        	 controllerpanel_tv.put("mute", (ImageView) findViewById(R.id.tv_mute));
        	 controllerpanel_tv.put("volumeup", (ImageView) findViewById(R.id.tv_volume_add));
        	 controllerpanel_tv.put("volumedown", (ImageView) findViewById(R.id.tv_volume_sub));
        	 controllerpanel_tv.put("backButton", (ImageView) findViewById(R.id.backButton));
        	 for(ImageView iv:controllerpanel_tv.values())	 iv.setOnClickListener(this);
        	 
        }else {
        	
	        setContentView(R.layout.activity_subhome);
	        list = (ListView) findViewById(R.id.lvlist);
	        String linghtString = "";

	        ArrayList array = new ArrayList();
	        for(LightContent light:dlist){
				final String[] codearray = light.getCode() .split(",");
				//筛选单个相同的设备，出去布防、模式、撤防不筛选
				if(codearray.length > 1){
					adapter=new LightAdapter(this, dlist);
					break;
				}else{
					if(!linghtString.equals(light.getCode() .substring(0,12))){
		        		linghtString = light.getCode() .substring(0,12);
		        		array.add(light);
		        	}
				}
	        }
	        if(adapter == null){
	        	adapter=new LightAdapter(this, array);
	        }
	 
	        list.setAdapter(adapter);
	        int check = type.indexOf("窗帘");
	        if(check == -1){
	        	upDateStatus();
	        }
	        
        }
        
        TextView title = (TextView)findViewById(R.id.txtTitle);
        title.setText(type+"-"+name);
        ImageButton btnNav = (ImageButton) findViewById(R.id.btnNav);
        btnNav.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				finish();		
			}
		});
    }

    private List<LightContent> getData(){
    	
    	dlist=new ArrayList<LightContent>();		
    
    	if(Global.getInstance().building.getmFloors().isEmpty()) 
    	{
    		Toast.makeText(this, "XML文件错误", Toast.LENGTH_LONG).show();
    		finish();
    		return dlist;
    	}

    	Floor floor = null ;
    	for(Floor fr : Global.getInstance().building.getmFloors())
       {
    	   if(fr.getName().equals(type))
    	   {
    		   floor  = fr;
    		   break;
    	   }
       }
	
    	if(floor == null) return dlist;
    
    	Room room =null;
    	
		for(Room rm:floor.getmRooms())
		{
			if(rm.getName().contains(name))
			{
				   room = rm;
				   break;
			}
		}
		
		if(room == null) return dlist;
		
		for(Sensor sensor:room.getmSensors())
		{	
			//TODO 修改每一行的标题字符串，只显示设备名称  dlist.add(new LightContent(room.getName()+"-"+sensor.getName(),false,sensor.getCode(),sensor.getQueryCode()));
			dlist.add(new LightContent(sensor.getName(),false,sensor.getCode(),sensor.getQueryCode()));
		}		
		return  dlist;
	}

    void upDateStatus()
    {
    	if( dlist.isEmpty() || type.equals("场景模式") ||type.equals("安防控制") ) return;
    	for(final LightContent light:dlist)  
    	//final LightContent light = dlist.get(0);
    	{
    		String code = light.getQueryCode();
    		if (code == null || code ==" ")  continue;
    		//send request code
    		final String[] codearray = code.split(",");
    		int index =0;					
    		for(String queryCode: codearray)
    		{
    			String newcode = new Tools().Parameterstring(queryCode);	
				if(newcode!=null)
				{
				//"3C060001FC010301BC"
					SendCommandRequest queryLight = new SendCommandRequest(newcode,new BackgroundServerResponseListener<JSONObject>()
						{
							@Override
							public void onServerResponse(JSONObject validResponse) {
								try {	
									String result = validResponse.getString("response");
									String hex = hexStr2Str(result);
									String status = hex.substring(13,15);
									if(status.equals("01"))    
									{
										light.setStatus(true);							
									}else{
										light.setStatus(false);
									} 
									   																			
									adapter.notifyDataSetInvalidated();
									
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}			
							}
							@Override
							public void onServerError(Throwable error) {
							
							}		
						});
				
				  ConnectManagerImpl.getInstance().SendCommandDelay(queryLight,(index)*150);
				  index++;
				}
    		}	
    	
    	}
    }
    

    /**  
     * 十六进制转换字符串 
     * @param String str Byte字符串(Byte之间无分隔符 如:[616C6B]) 
     * @return String 对应的字符串 
     */    
    public static String hexStr2Str(String hexStr)  
    {    
        String str = "0123456789ABCDEF";    
        char[] hexs = hexStr.toCharArray();    
        byte[] bytes = new byte[hexStr.length() / 2];    
        int n;    
  
        for (int i = 0; i < bytes.length; i++)  
        {    
            n = str.indexOf(hexs[2 * i]) * 16;    
            n += str.indexOf(hexs[2 * i + 1]);    
            bytes[i] = (byte) (n & 0xff);    
        }    
        return new String(bytes);    
    }
    
	@Override
	public void onClick(View v) {	
		//below is cooler
		if(v.getId() == R.id.value_up_button && temperature != 30){
			temperature++;
			temperature_view.setText(""+temperature + "℃" );
		}else if(v.getId() == R.id.value_down_button && temperature != 16){
			temperature--;
			temperature_view.setText(""+temperature + "℃" );
		}
		//below is TV
		if(v.getId() == R.id.tv_power ){
			poweronoff = !poweronoff;
		}
		start(getCode(v.getId()));		
	}
	
	String getNamebyId(int Id)
	{
		String name = null;
		switch(Id)		
		{
		//cooler
			case R.id.openPower:
				//"空调开";
				name = "280100,OD0100,1C0100";
				break;
			case R.id.ClosePower:
				//"空调关";
				name = "280200,OD0200,1C0200";
				break;
			case R.id.ch_button:
				//"空调制冷";
				name = "280401,OD0401,1C0401";
				break;
			case R.id.hc_button:
				//"空调制热";
				name = "280402,OD0402,1C0402";
				break;
			case R.id.auto_button:
				//"空调自动";
				name = "280304,OD0304,1C0304";
				break;
			case R.id.fcu_button:
				//"空调通风";
				name = "280403,OD0403,1C0403";
				break;
			case R.id.wind_minButton:
				//"空调小风";
				name = "280303,OD0303,1C0303";
				break;
			case R.id.wind_inButton:
				// "空调中风";
				name = "280302,OD0302,1C0302";
				break;
			case R.id.wind_maxButton:
				//"空调大风";
				name = "280301,OD0301,1C0301";
				break;
			case R.id.value_up_button:
				//"空调温度+";
				name = "280701,OD0701,1C0701";
				break;
			case R.id.value_down_button:
				//"空调温度-";
				name = "280702,OD0702,1C0702";
				break;
			//TV 小米盒子 都是通过一个xml
			case R.id.tv_power:
				//name = "TV开关"; 292000,OB2000,OC2000,
				name = "开关,开关机";
				break;
			case R.id.tv_signal:
				//信号输入  292001,OB2001,OC2001,
				name = "信号输入";
				break;
			case R.id.tv_upkey:
				//"上";  292002,OB2002,OC2002,
				name = "上";
				break;
			case R.id.tv_leftkey:
				//"左";  292008,OB2008,OC2008,
				name = "左";
				break;
			case R.id.tv_okkey:
				//"确认";  292020,OB2020,OC2020,
				name = "确认";
				break;
			case R.id.tv_rightkey:
				//"右"; 292010,OB2010,OC2010,
				name = "右";
				break;
			case R.id.tv_home:
				//"菜单"; 292040,OB2040,OC2040,
				name = "首页";
				break;
			case R.id.tv_dwonkey:
				//"下"; 292004,OB2004,OC2004,
				name = "下";
				break;
			case R.id.tv_mute:
				//"静音"; 292400,OB2400,OC2400,
				name = "静音";
				break;
			case R.id.tv_volume_add:
				//"音量+"; 292100,OB2100,OC2100,
				name = "音量+";
				break;
			case R.id.tv_volume_sub:
				//"音量-"; 292200,OB2200,OC2200,
				name = "音量-";
				break;
			case R.id.backButton:
				//"返回"; 292800,OB2800,OC2800,
				name = "返回";
				break;
			//窗帘
			case R.id.curtainOpen:
				//"窗帘开(O)";
				name = "1E0201,0A0101,1E0101";
				break;
			case R.id.curtainStop:
				//"窗帘停(S)";
				name = "1E0200,0A0100,1E0100";
				break;
			case R.id.curtainClose:
				//"窗帘关(C)";
				name = "1E0202,0A0102,1E0102";
				break;
		}
		return name;
	}
	
	String getCode(int Id)
	{
		for(LightContent light:dlist){
			String  subcode = light.getCode().substring(2,4)+light.getCode().substring(10,14);
			final String[] codearray = getNamebyId(Id).split(",");
			for(String device_code: codearray){
				final String copy_device_code = device_code;
				final String namestring = light.getName();
				if(subcode.equals(copy_device_code) || copy_device_code.equals(namestring)){
					//"280701,OD0701,1C0701"
					if(subcode.equals("280701") || subcode.equals("OD0701") || subcode.equals("1C0701") || subcode.equals("280702") || subcode.equals("OD0702") || subcode.equals("1C0702")){
						String sendTemper = light.getCode() .substring(0,10)+"06"+String.format("%X",temperature)+"00";
						return  sendTemper;
					}
					return  light.getCode();
				}
				
			}			
		}		
		return null;	
	}
	
	void start(final String code)
	{
		if (code == null)  return;
		//send request code
		final String[] codearray = code.split(",");
							
		for(String cc: codearray)
		{
			final String ccc = cc;
		
			new Thread(new Runnable(){
				@Override
				public void run() {					
					String newcode = new Tools().Parameterstring(ccc);	
					if(newcode!=null)
					{
					//"3C060001FC010301BC"
						SendCommandRequest openLight = new SendCommandRequest(newcode,new ServerResponseListener<JSONObject>()
							{
								@Override
								public void onServerResponse(JSONObject validResponse) {
									try {	
										Log.i("onServerResponse",validResponse.getString("status"));
									if(ccc.equals(codearray[0]))
										Log.i(type,"成功开启:" + code );
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}			
								}
								@Override
								public void onServerError(Throwable error) {
									if(ccc.equals(codearray[0]))
										Log.e(type,"开启失败:"+ code);
								}		
							});
					
					  ConnectManagerImpl.getInstance().SendCommand(openLight);
					}
					
				}}).start();				
		}	
	}

}
