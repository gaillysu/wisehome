package com.toplion.wisehome.global;

import com.toplion.wisehome.model.Building;

import android.content.Context;





public class Global {
	
	private static Global sharedObject = null;
	
	public Building building;
	
	private Global(){		
		building = new Building();
	}

	/*
	 * singleton Class
	 */
	public static Global getInstance()
	{
		if(null == sharedObject)
		{
			sharedObject = new Global();
		}
		return sharedObject;
	}

}
