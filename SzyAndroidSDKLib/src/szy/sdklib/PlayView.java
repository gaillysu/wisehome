package szy.sdklib;

import java.nio.ByteBuffer;

import szy.dataserver.DataDispose;
import szy.dataserver.SdkHandle;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Bitmap.Config;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;

public class PlayView extends View {
	private int mIntPlayWidth;
	private int mIntPlayHeight;
	private int mIntSrcWidth = 0;
	private int mIntSrcHeight = 0;
    private Matrix matrixScale = null; 
	private Bitmap videoBitmap = null;
	private DataDispose mDataDispose;
	
	private AudioTrack audioTrack = null;
	private int mIntSamplePerSec;
	private int mIntBitPerSample;
	private int mIntChannel;
	
	private Handler mHandler;

	public PlayView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	public PlayView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public PlayView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public void SetPlayWH(int nWidth, int nHeight) {
		mIntPlayWidth = nWidth;
		mIntPlayHeight = nHeight;
		if (mIntSrcHeight>0 && mIntSrcWidth>0) {
			matrixScale = new Matrix();
			float scaleWidth = (float) mIntPlayWidth / mIntSrcWidth; // 按固定大小缩放
			float scaleHeight = (float) mIntPlayHeight / mIntSrcHeight;
			matrixScale.postScale(scaleWidth, scaleHeight);// 产生缩放后的Bitmap对象
		}
	}
	
	public void setHandle(Handler handler){
		this.mHandler = handler;
	}
	
	public void StartPlay(String strIpAddress, int nPort, String sxtID,String sxtRzm) {
		mDataDispose = new DataDispose();
		mDataDispose.SetSdkHandle(mSdkHandle);
		mDataDispose.Start(strIpAddress, nPort, sxtID, sxtRzm);
	}
	
	public void StartPlay(String strIpAddress, int nPort, String sxtID,String username,String password) {
		mDataDispose = new DataDispose();
		mDataDispose.SetSdkHandle(mSdkHandle);
		mDataDispose.Start(strIpAddress, nPort, sxtID, username,password);
	}

	// 上：(7,1,speed)
	// 下：(7,2,speed)
	// 左：(7,3,speed)
	// 右：(7,4,speed)
	// 旋转停止:(7,0,0)
	// 变倍(远):(4,2,speed)
	// 变倍(近):(4,1,speed)
	// 变倍停止:(4,0,0)
	public void SendPtzCmd(int nFunc,int nCtrl,int nSpeed) {
		if (mDataDispose!=null) {
			mDataDispose.SendPTZCommand(nFunc, nCtrl, nSpeed);
		}
	}
	
	public void Stop(){
		if (mDataDispose!=null) {
			mDataDispose.Stop();
		}
		if (audioTrack!=null) {
			try {
				audioTrack.stop();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			}
			audioTrack.release();
			audioTrack = null;
		}
		
		try {
			if (videoBitmap != null && !videoBitmap.isRecycled()) {
				videoBitmap.recycle();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		mIntSrcWidth = 0;
		mIntSrcHeight = 0;
		postInvalidate();
	}

	private SdkHandle mSdkHandle = new SdkHandle() {
		@Override
		public void videoDataCallback(byte[] szData, int nLen, int nWidth,
				int nHeight) {
			if (nWidth!=mIntSrcWidth || nHeight!=mIntSrcHeight) {
				if (mHandler!=null) {
					mHandler.sendEmptyMessage(100);
				}
				mIntSrcWidth = nWidth;
				mIntSrcHeight = nHeight;
				matrixScale = null;
				matrixScale = new Matrix();
				float scaleWidth = (float) mIntPlayWidth / nWidth; // 按固定大小缩放
				float scaleHeight = (float) mIntPlayHeight / nHeight;
				matrixScale.postScale(scaleWidth, scaleHeight);// 产生缩放后的Bitmap对象
			}

			videoBitmap = Bitmap.createBitmap(nWidth, nHeight, Config.RGB_565);
			videoBitmap.copyPixelsFromBuffer(ByteBuffer.wrap(szData,0,nLen));
			postInvalidate();
		}
		
		@Override
		public void audioDataCallback(byte[] szData, int nLen, int nSamplePerSec,
				int nBitPerSample, int nChannel) {
			try {
				if (nSamplePerSec!=mIntSamplePerSec || nBitPerSample!=mIntBitPerSample
						|| nChannel!=mIntChannel) {
					mIntSamplePerSec = nSamplePerSec;
					mIntBitPerSample = nBitPerSample;
					mIntChannel = nChannel;
					if (audioTrack!=null) {
						try {
							audioTrack.stop();
						} catch (IllegalStateException e) {
							e.printStackTrace();
						}
						audioTrack.release();
						audioTrack = null;
					}
					int audioEncoding = nBitPerSample==16 ? AudioFormat.ENCODING_PCM_16BIT
							: AudioFormat.ENCODING_PCM_8BIT;
					int channelConfiguration = nChannel==1 ? AudioFormat.CHANNEL_OUT_MONO
							: AudioFormat.CHANNEL_OUT_STEREO;
					int minBufferSize = AudioTrack.getMinBufferSize(nSamplePerSec,channelConfiguration,
							audioEncoding);
					audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, nSamplePerSec,
							channelConfiguration,audioEncoding, minBufferSize*2,
							AudioTrack.MODE_STREAM);
					audioTrack.play();
				}
				audioTrack.write(szData, 0, nLen);
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}

		@Override
		public void errorCallback(int nError, int nExtra) {
			switch (nError) {
			case -1:
				//超过观看人数上限nExtra人
				Message msg = new Message();
				msg.what = -1;
				msg.arg1 = nExtra;
				mHandler.sendMessage(msg);
				break;
			case -2:
				//摄像头认证码不对
				mHandler.sendEmptyMessage(-2);
				break;
			case -3:
				//音视频数据接收失败
				mHandler.sendEmptyMessage(-3);
				break;
			case -4:
				//连接服务器失败
				mHandler.sendEmptyMessage(-4);
				break;
			case -5:
				//服务器认证失败
				mHandler.sendEmptyMessage(-5);
				break;
			default:
				break;
			}
			Stop();
		}
		
	};
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		try {
			Bitmap resizeBitmap = Bitmap.createBitmap(videoBitmap, 0, 0,
					videoBitmap.getWidth(), videoBitmap.getHeight(), matrixScale,
					false);
			canvas.drawBitmap(resizeBitmap, 0, 0, null);
			resizeBitmap.recycle();

//			if (videoBitmap != null && !videoBitmap.isRecycled()) {
//				videoBitmap.recycle();
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		//显示完毕，继续解码回调
		if (mDataDispose!=null) {
			mDataDispose.NextVideoDecode();
		}
	}
}
