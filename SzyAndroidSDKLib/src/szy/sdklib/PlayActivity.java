package szy.sdklib;

import szy.sdklib.RepeatingButton.OnTouchOnoffListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PlayActivity extends Activity {
	private PlayView mPlayView;
//	private SeekBar mSeekBar;
//	private final int MAXPTZSPEED = 64;
	
	private boolean mBoolPtz = false;
	private boolean mBoolStop = false;
	
	private LinearLayout mLayoutTitle;
	private LinearLayout mLayoutPlay;
	private LinearLayout mLayoutProgress;
	private LinearLayout mLayoutPtz;
	
	private String strID;
	private String strIP;
	private int nPort;
	private String strUsername;
	private String strPassword;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().setFormat(PixelFormat.TRANSLUCENT);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

		setContentView(R.layout.play);

		strID = getIntent().getStringExtra("ID");
		String strName = getIntent().getStringExtra("NAME");
		mBoolPtz = getIntent().getBooleanExtra("PTZ", false);
		strIP = getIntent().getStringExtra("IP");
		nPort = getIntent().getIntExtra("PORT", 0);
		strUsername = getIntent().getStringExtra("USERNAME");
		strPassword = getIntent().getStringExtra("PASSWORD");
		
		RepeatingButton btnUp = (RepeatingButton) findViewById(R.id.ptz_up);
		RepeatingButton btnDown = (RepeatingButton) findViewById(R.id.ptz_down);
		RepeatingButton btnLeft = (RepeatingButton) findViewById(R.id.ptz_left);
		RepeatingButton btnRight = (RepeatingButton) findViewById(R.id.ptz_right);
		RepeatingButton btnZoomin = (RepeatingButton) findViewById(R.id.ptz_zoomin);
		RepeatingButton btnZoomout = (RepeatingButton) findViewById(R.id.ptz_zoomout);
		btnUp.SetOnTouchOnoffListener(mOnoffListener);
		btnDown.SetOnTouchOnoffListener(mOnoffListener);
		btnLeft.SetOnTouchOnoffListener(mOnoffListener);
		btnRight.SetOnTouchOnoffListener(mOnoffListener);
		btnZoomin.SetOnTouchOnoffListener(mOnoffListener);
		btnZoomout.SetOnTouchOnoffListener(mOnoffListener);
		if (!mBoolPtz) {
			btnUp.setEnabled(false);
			btnDown.setEnabled(false);
			btnLeft.setEnabled(false);
			btnRight.setEnabled(false);
			btnZoomin.setEnabled(false);
			btnZoomout.setEnabled(false);
		}
		
		TextView textView = (TextView) findViewById(R.id.text_title);
		textView.setText(strName);
		
		mLayoutTitle = (LinearLayout) findViewById(R.id.llayout_title);
		mLayoutPlay = (LinearLayout) findViewById(R.id.llayout_playview);
		mLayoutPtz = (LinearLayout) findViewById(R.id.llayout_playyuntai);
		mLayoutProgress = (LinearLayout) findViewById(R.id.llayout_progress);
		Display dis = getWindowManager().getDefaultDisplay();
		LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
				dis.getWidth(),dis.getWidth()*3/4);
		mPlayView = new PlayView(this);
		mPlayView.SetPlayWH(dis.getWidth(),dis.getWidth()*3/4);
		mPlayView.setHandle(mHandler);
		mLayoutPlay.addView(mPlayView, lParams);

		mPlayView.StartPlay(strIP, nPort, strID, strUsername, strPassword);
		showProgress();
		mBoolStop = false;
		
		Button btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mBoolStop = true;
				if (mPlayView!=null) {
					mPlayView.Stop();
				}
				PlayActivity.this.finish();
			}
		});
		
		final ImageButton btnPlay = (ImageButton) findViewById(R.id.btn_play);
		btnPlay.setBackgroundResource(R.drawable.pause);
		btnPlay.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mBoolStop) {
					mBoolStop = false;
					showProgress();
					if (mPlayView!=null) {
						mPlayView.StartPlay(strIP, nPort, strID, strUsername, strPassword);
					}
					btnPlay.setBackgroundResource(R.drawable.pause);
				}else {
					mBoolStop = true;
					hideProgress();
					if (mPlayView!=null) {
						mPlayView.Stop();
					}
					btnPlay.setBackgroundResource(R.drawable.play);
				}
			}
		});
	}
	
	private Handler mHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case 100:
				hideProgress();
				break;
			case -1:
				//超过观看人数上限
				String strMsg = getResources().getString(R.string.gankanshangxian).replace("%d", msg.arg1+"");
				showTipsDlg(strMsg);
				break;
			case -2:
				//摄像头认证码不对
				showTipsDlg2(R.string.sxtyanzhengshibai);
				break;
			case -3:
				//音视频数据接收失败
				showTipsDlg2(R.string.shujujieshousb);
				break;
			case -4:
				//连接服务器失败
				showTipsDlg2(R.string.fuwuqilianjiesb);
				break;
			case -5:
				//服务器认证摄像头失败
				showTipsDlg2(R.string.sxtrenzhengbtg);
				break;
			case -6:
				//服务器拒绝认证
				showTipsDlg2(R.string.fuwuqijujuerz);
				break;
			default:
				break;
			}
		}
	};
	
	private void showTipsDlg2(int nMsg) {
		if (!mBoolStop) {
			hideProgress();
			AlertDialog.Builder builder = new Builder(PlayActivity.this);
			builder.setMessage(nMsg);
			builder.setTitle(R.string.dlg_tishi);
			builder.setCancelable(false);
			builder.setPositiveButton(R.string.btn_queding,
					new android.content.DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			builder.create().show();
		}
	}
	
	private void showTipsDlg(String sMsg) {
		if (!mBoolStop) {
			hideProgress();
			AlertDialog.Builder builder = new Builder(PlayActivity.this);
			builder.setMessage(sMsg);
			builder.setTitle(R.string.dlg_tishi);
			builder.setCancelable(false);
			builder.setPositiveButton(R.string.btn_queding,
					new android.content.DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			builder.create().show();
		}
	}
	
	private void showProgress() {
		mLayoutProgress.setVisibility(View.VISIBLE);
		mPlayView.setVisibility(View.GONE);
	}

	private void hideProgress() {
		mLayoutProgress.setVisibility(View.GONE);
		mPlayView.setVisibility(View.VISIBLE);
	}
	
	private OnTouchOnoffListener mOnoffListener = new OnTouchOnoffListener() {
		@Override
		public void OnTouchOnoff(View view, int nMotionEvent) {
			if (mPlayView==null) {
				return;
			}
			Log.d("1234", "PTZ:"+view.getId()+"-"+nMotionEvent);
			int nSpeed = 32;
			if (nMotionEvent==MotionEvent.ACTION_DOWN) {
				if (view.getId()==R.id.ptz_up) {
					mPlayView.SendPtzCmd(7,1,nSpeed);
				}else if (view.getId()==R.id.ptz_down) {
					mPlayView.SendPtzCmd(7,2,nSpeed);
				}else if (view.getId()==R.id.ptz_left) {
					mPlayView.SendPtzCmd(7,3,nSpeed);
				}else if (view.getId()==R.id.ptz_right) {
					mPlayView.SendPtzCmd(7,4,nSpeed);
				}else if (view.getId()==R.id.ptz_zoomin) {
					mPlayView.SendPtzCmd(4,1,nSpeed);
				}else if (view.getId()==R.id.ptz_zoomout) {
					mPlayView.SendPtzCmd(4,2,nSpeed);
				}
			}else if (nMotionEvent==MotionEvent.ACTION_UP) {
				if (view.getId()==R.id.ptz_up) {
					mPlayView.SendPtzCmd(7,0,0);
				}else if (view.getId()==R.id.ptz_down) {
					mPlayView.SendPtzCmd(7,0,0);
				}else if (view.getId()==R.id.ptz_left) {
					mPlayView.SendPtzCmd(7,0,0);
				}else if (view.getId()==R.id.ptz_right) {
					mPlayView.SendPtzCmd(7,0,0);
				}else if (view.getId()==R.id.ptz_zoomin) {
					mPlayView.SendPtzCmd(4,0,0);
				}else if (view.getId()==R.id.ptz_zoomout) {
					mPlayView.SendPtzCmd(4,0,0);
				}
			}
			
		}
	};
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			mBoolStop = true;
			if (mPlayView!=null) {
				mPlayView.Stop();
			}
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		Display dis = getWindowManager().getDefaultDisplay();
		mLayoutPlay.removeView(mPlayView);
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			mLayoutTitle.setVisibility(View.GONE);
			mLayoutPtz.setVisibility(View.GONE);
			if (mPlayView != null) {
				LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
						dis.getWidth(), dis.getHeight());
				mPlayView.SetPlayWH(dis.getWidth(), dis.getHeight());
				mLayoutPlay.addView(mPlayView, lParams);
				mPlayView.postInvalidate();
			}
		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			mLayoutPtz.setVisibility(View.VISIBLE);
			mLayoutTitle.setVisibility(View.VISIBLE);
			mLayoutTitle.setVisibility(View.VISIBLE);
			if (mPlayView != null) {
				LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
						dis.getWidth(),dis.getWidth()*3/4);
				mPlayView.SetPlayWH(dis.getWidth(),dis.getWidth()*3/4);
				mLayoutPlay.addView(mPlayView, lParams);
				mPlayView.postInvalidate();
			}
		}
	}
}
